#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, time, cbor
from binascii import hexlify

submission_id = os.urandom(32)
submission_id_hex = hexlify(submission_id).decode()

header = {"type": "as->vst",
          "created_at": int(time.time()), 
          "submission_id": submission_id,
          "call_back_url": "https://aktensystem-1.telematik/"}

lp1 = os.urandom(103)
an1 = os.urandom(32)

pre_submission = [header, [lp1, an1]]

submission = cbor.dumps(pre_submission)

print(len(submission))

