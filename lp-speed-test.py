#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import secrets

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

from progressbar import progressbar


if __name__ == '__main__':

    # nur zum Test, dummy-Key
    #vst_private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
    vst_private_key = ec.generate_private_key(ec.SECP256R1(), default_backend())
    vst_public_key = vst_private_key.public_key()

    ANZAHL = 1_300_000

    # mal zum testen was 1,3 Millionen zu durchlaufen kostet
    print(f"Ich erzeuge {ANZAHL} ANs.")
    for _ in progressbar(range(0,1_300_000)):
        test_an = secrets.token_bytes(32)

    print(f"Ich erzeuge {ANZAHL} LPs.")
    # jetzt geht es richtig los
    for _ in progressbar(range(0,1_300_000)):
        # Jetzt verschlüssele ich als AS (Komponente Autorisierung) die KVNR für die VST.
        # Das Chiffrat ist nach Pseudonymisierungskonzept das Lieferpseudonym (LP).

        #private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
        private_key = ec.generate_private_key(ec.SECP256R1(), default_backend())
        pn = private_key.public_key().public_numbers()
        shared_secret = private_key.exchange(ec.ECDH(), vst_public_key)
        hkdf = HKDF(algorithm=hashes.SHA256(), length=32, salt=None, info=b'', backend=default_backend())
        aes_key = hkdf.derive(shared_secret)
        plaintext = b"A123456789"
        iv = secrets.token_bytes(12)
        ciphertext_aes = AESGCM(aes_key).encrypt(iv, plaintext, associated_data=None)
        x = pn.x.to_bytes(32, signed=False, byteorder="big")
        y = pn.y.to_bytes(32, signed=False, byteorder="big")

        lp = b'\x01' + x + y + iv + ciphertext_aes

