# Speed-Test AN und LP erzeugen

## 1.300.000 AN und LP erzeugen mit NIST-P-256

	$ ./lp-speed-test.py 
	Ich erzeuge 1300000 ANs.
	100% (1300000 of 1300000) |######################| Elapsed Time: 0:00:03 Time:  0:00:03
	Ich erzeuge 1300000 LPs.
	100% (1300000 of 1300000) |######################| Elapsed Time: 0:06:08 Time:  0:06:08


## 1.300.000 AN und LP erzeugen mit BrainpoolP256r1

(Im Code ändere ich SECP -> Brainpool.)

	$ ./lp-speed-test.py 
	Ich erzeuge 1300000 ANs.
	100% (1300000 of 1300000) |######################| Elapsed Time: 0:00:03 Time:  0:00:03
	Ich erzeuge 1300000 LPs.
	100% (1300000 of 1300000) |######################| Elapsed Time: 0:21:42 Time:  0:21:42


## 1.300.000 AN mal direkt über dd auslesen

	$ time dd if=/dev/urandom bs=32 of=/dev/null count=1300000
	1300000+0 Datensätze ein
	1300000+0 Datensätze aus
	41600000 Bytes (42 MB, 40 MiB) kopiert, 3,38598 s, 12,3 MB/s

	real	0m3,387s
	user	0m1,473s
	sys	0m1,913s

